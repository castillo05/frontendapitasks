import { Component, OnInit } from '@angular/core';
import {Task} from './models/task.models';
import {GLOBAL} from './service/GLOBAL';
import{TasksService} from './service/tasks.service';
import {FormControl,FormGroup,Validators} from '@angular/forms'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'charla-tasks';
  public Tasks : Task[]=[];
  public Task: Task;
  public url:string;
  public formTask: FormGroup;
  // alertas de mensajes
  public message:string;
  public alert: boolean=false;

  // Variables para ngxpagination
  p: number = 1;


  constructor(private taskservice: TasksService) {
    this.url= GLOBAL.url;
    this.Task = new Task('','','');
    this.formTask = new FormGroup({
      'title': new FormControl('', Validators.required),
      'description': new FormControl('', Validators.required)
    });

  }

  ngOnInit(){
    this.getTasks();
  }
  // Mostrar tareas agregadas
  getTasks(){
    this.taskservice.getTasks().subscribe((res:any)=>{
      if(!res) return console.log('Hey error con la respuesta del servisor');

      this.Tasks=res;
      console.log(this.Tasks);
    },error=>{
      let errorMessage=<any>error;
      let body= JSON.stringify(error.message);
      if(errorMessage){
        this.message=body;
        this.alert=true;
        setTimeout(()=>this.alert=false,5000);
        console.log(error);
      }
    });
  }

  submit(){
    console.log(this.formTask.value);
    this.taskservice.newTask(this.formTask.value).subscribe((res:any)=>{
      if(!res) return console.log('Hey error con la respuesta del servidor');

      if (res) {

        this.message='Tarea agregada con exito';
        this.alert = true;
        setTimeout(()=>this.alert=false,5000);
        console.log(this.message);
        this.formTask.reset(
          {
            'titulo':"",
            'descripcion':""
          }
        );
        this.getTasks();
      }
    },error=>{
      let errorMessage=<any>error;
      let body= JSON.stringify(error.message);
      if(errorMessage){
        this.message=body;
        this.alert=true;
        setTimeout(()=>this.alert=false,5000);
        console.log(error);
      }
    });
  }

  // Mostrar una tarea
  getTask(_id:string){
    this.taskservice.getTask(_id).subscribe((res:any)=>{
      if (!res) return console.log('Hey error con la respuesta del servidor');

      this.Task=res;
      console.log(this.Task);
    },error=>{
      let errorMessage=<any>error;
      let body= JSON.stringify(error.message);
      if(errorMessage){
        this.message=body;
        this.alert=true;
        setTimeout(()=>this.alert=false,5000);
        console.log(error);
      }
    });
  }

  updateTask(_id:string){
    this.taskservice.updateTask(this.Task,_id).subscribe((res:any)=>{
      if (!res) return console.log('Hey error con la respuesta del servidor');
      this.message='Tarea actualizada con exito';
      this.alert = true;
      setTimeout(()=>this.alert=false,5000);
      this.getTasks();
      console.log(res);

    },error=>{
      let errorMessage=<any>error;
      let body= JSON.stringify(error.message);
      if(errorMessage){
        this.message=body;
        this.alert=true;
        setTimeout(()=>this.alert=false,5000);
        console.log(error);
      }
    });
  }

  deleteTask(_id:string){
    this.taskservice.deleteTask(_id).subscribe((res:any)=>{
      const confirmation=confirm('Seguro que quieres eliminar esta tarea?');
      if(confirmation) {
        if (!res) return console.log('Hey error con la respuesta del servidor');

        this.message= 'Tarea eliminada con exito';
        this.alert = true;
        setTimeout(()=>this.alert=false,5000);
        this.getTasks();
      }

    },error=>{
      let errorMessage=<any>error;
      let body= JSON.stringify(error.message);
      if(errorMessage){
        this.message=body;
        this.alert=true;
        setTimeout(()=>this.alert=false,5000);
        console.log(error);
      }
    });
  }

}
