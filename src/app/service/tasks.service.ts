import { Injectable } from '@angular/core';
import {GLOBAL} from './GLOBAL';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Task} from '../models/task.models';

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  public url: string;
  constructor(private http:HttpClient) {
    this.url=GLOBAL.url;
  }

  // Guardar tareas
  newTask(task){
    const json = JSON.stringify(task);
    const params = json;
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    return this.http.post(this.url,params,{headers});
  }

  // Mostrar tareas agregadas
  getTasks(){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    return this.http.get(this.url,{headers});
  }

  // Mostrar una tarea
  getTask(id:string){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    return this.http.get(this.url+'/'+id,{headers});
  }

  // Actualizar Tarea
  updateTask(Task: Task,id:string){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    const task = JSON.stringify(Task);

    return this.http.put(this.url+'/'+id,Task,{headers});
  }

  deleteTask(id:string){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    return this.http.delete(this.url+'/'+id,{headers});
  }
}
